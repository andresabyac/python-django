from django.contrib import admin
from .models import Departamento
# Register your models here.

class EmpleadoAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'short_name',
        
    )

admin.site.register(Departamento)
