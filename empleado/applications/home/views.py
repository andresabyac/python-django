from django.shortcuts import render
#views generics 'TemplateView'
from django.views.generic import (
    TemplateView, 
    ListView, 
    CreateView
)
#import models
from .models import Prueba
from .forms import PruebaForm

# Create your views here.
class PruebaView(TemplateView):
    template_name = 'home/prueba.html'

class ResumeFoundationView(TemplateView):
    template_name = 'home/resume_foundation.html'


class PruebaListView(ListView):
    template_name = "home/lista.html"
    context_object_name = 'listanumeros'
    queryset = ['0','2','3','5','6']

class ListarPrueba(ListView):
    template_name = "home/lista_prueba.html"
    model = Prueba
    context_object_name = 'lista'


class PruebaCreateView(CreateView):
    template_name = "home/add.html"
    model = Prueba
    #generate fields form PruebaForm
    form_class= PruebaForm
    success_url = '/'
    