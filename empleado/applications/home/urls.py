from django.contrib import admin
from django.urls import path
from django.urls.conf import include
# ' . ' means file in same level
from . import views

urlpatterns = [
    #'.as_view() ' method when is used generic view 
    path('prueba/', views.PruebaView.as_view()),
    path('lista/',views.PruebaListView.as_view()),
    path('lista-prueba/',views.ListarPrueba.as_view()),
    path(
        'add/',
        views.PruebaCreateView.as_view(),
        name='prueba_add'
    ),
    path(
        'resumen-foundation/',
        views.ResumeFoundationView.as_view(),
        name='resume_foundation'
    ),
]