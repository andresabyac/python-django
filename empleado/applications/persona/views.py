from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    TemplateView,
    UpdateView,
    DeleteView,
)
#1.- Listar todos los empleados de la empresa
#2.- Listar todos los empleados que perteneces a un area de la empresa
#3.- Listar empleados por trabajo
#4.- Listar los empleados por la palabra clave
#5.- listar habilidades de un empleado
#models
from .models import Empleado
# Create your views here.

#class of extends templates html 

class InicioView(TemplateView):
    '''Vista que carga la pagina de inicio'''
    template_name = 'inicio.html'

#1.- Listar todos los empleados de la empresa
class ListAllEmpleados(ListView):
    template_name = 'persona/list_all.html'
    paginate_by = 4
    ordering= 'id'
    
    def get_queryset(self):
        key_word= self.request.GET.get("kword",'')
        lista = Empleado.objects.filter(
            full_name__icontains = key_word
        )
        print("La lista resultante es: ",lista)
        return lista
    
#2.- Listar todos los empleados que perteneces a un area de la empresa
class ListByArea(ListView):
    template_name = 'persona/list_by_area.html'
    context_object_name='empleados'
    
    #return a employees list by filter short_name
    def get_queryset(self):
        area = self.kwargs['shortname']
        print("Kwargs: ", area)
        lista = Empleado.objects.filter(
            departamento__short_name = area
        )
        print("La lista es : ", lista)
        return lista

class ListEmpleadosByKword(ListView):
    # employees list by kword
    template_name= 'persona/by_kword.html'
    context_object_name = 'empleado'
    
    def get_queryset(self):
        print("**********************************")
        key_word= self.request.GET.get("kword",'')
        print("Request: ",self.request)
        print(key_word)
        print("==================================")
        lista = Empleado.objects.filter(
        first_name = key_word
        )
        print("La lista resultante es: ",lista)
        return lista
    
class ListSkillsEmployee(ListView):
    template_name = 'persona/skill.html'
    context_object_name = 'skills'
    
    def get_queryset(self):
        #return one employee
        id_employee = self.request.GET.get("kword",'')
        print("id employee",id_employee)
        empleado = Empleado.objects.get(id=id_employee)
        #print(" Habilidades del empleado: \n",empleado.habilidades.all())
        
        return empleado.habilidades.all()


class EmpleadoDetailView(DetailView):
    model = Empleado
    template_name = "persona/detail_employee.html"
    context_object_name='empleado'
    
    
    def get_context_data(self, **kwargs):
        context = super(EmpleadoDetailView, self).get_context_data(**kwargs)
        context['titulo']=' Empleado del mes'
        return context
    

class SucessView(TemplateView):
    template_name = "persona/sucess.html"


class EmpleadoCreateView(CreateView):
    model = Empleado
    template_name = "persona/add_empleado.html"
    fields = [
        'first_name',
        'last_name',
        'job',
        'departamento',
        'habilidades',
        'avatar',
        ]#('__all__')
    #self direction
    success_url = reverse_lazy('persona_app:empleado_admin')
    #track the form and update fields full_name
    def form_valid(self, form):
        empleado = form.save()#content all data of form employee
        empleado.full_name = empleado.first_name + ' ' + empleado.last_name
        empleado.save()
        #methode override class EmpleadoCreateView
        return super(EmpleadoCreateView, self).form_valid(form)




class EmpleadoUpdateView(UpdateView):
    model = Empleado
    template_name = "persona/update.html"
    fields = [
        'first_name',
        'last_name',
        'job',
        'departamento',
        'habilidades',
        ]
    success_url= reverse_lazy('persona_app:empleado_admin')
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        print("================METHODE POST================")
        #retrieve a tupla of POST request
        print(request.POST['last_name'])
        return super().post(request, *args, **kwargs)
    
    def form_valid(self, form):
        print("================METHODE FORM VALID================")
        return super(EmpleadoUpdateView, self).form_valid(form)
    

class EmpleadoDeleteView(DeleteView):
    model = Empleado
    template_name = "persona/delete.html"
    success_url= reverse_lazy('persona_app:empleado_admin')


#--------------------------------------------------------------------------
class ListAllEdmin(ListView):
    template_name = 'persona/list_all_admin.html'
    paginate_by = 10
    ordering= 'id'
    context_object_name='empleados'
    model=Empleado
    