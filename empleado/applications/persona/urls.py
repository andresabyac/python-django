from django.contrib import admin
from django.urls import path

from . import views

app_name = "persona_app"

urlpatterns = [
    path(
        '',
        views.InicioView.as_view(),
        name='inicio'
        ),
    #'.as_view() ' method when is used generic view 
    #1.- Listar todos los empleados de la empresa
    path(
        'list-all-employee/',
        views.ListAllEmpleados.as_view(),
        name ='empleado_all'
    ),
    #2.- Listar todos los empleados que perteneces a un area de la empresa
    path(
        'list-by-area/<shortname>/',
        views.ListByArea.as_view(),
        name='empleados_area'
        ),
    #search a employee by kword
    path('search-employee/',views.ListEmpleadosByKword.as_view()),
    #list employee skills
    path('skills-employee/',views.ListSkillsEmployee.as_view()),
    #Detailview 
    path(
        'see-employee/<pk>/',
        views.EmpleadoDetailView.as_view(),
        name='empleado_detail'
        ),
    #CreateView
    path(
        'add-employee/',
        views.EmpleadoCreateView.as_view(),
        name='empleado_add'
    ),
    #templateview need a template is used call other template 
    path(
        'sucess/',
        views.SucessView.as_view(),
        name='correcto'
        ),
    path(
        'update-employee/<pk>/',
        views.EmpleadoUpdateView.as_view(),
        name='modificar_empleado'
        ),
    path(
        'delete-employee/<pk>/',
        views.EmpleadoDeleteView.as_view(),
        name='borrar_empleado'
        ),
    path(
        'list-employee-admin/',
        views.ListAllEdmin.as_view(),
        name='empleado_admin'
        ),
    
]