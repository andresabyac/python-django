from django.db import models
from ckeditor.fields import RichTextField
from applications.departamento.models import Departamento

class Habilidades(models.Model):
    habilidad = models.CharField('Habilidad', max_length=50)
    
    class Meta:
        verbose_name= 'Habilidad'
        verbose_name_plural= 'Habilidad Empleados'
    
    def __str__(self) -> str:
        return str(self.id)+' - ' + self.habilidad

# Create your models here.
class Empleado(models.Model):
    '''Model for employee table'''
    '''Contador, Administrador, Economista'''
    JOB_CHOICES=(
        ('0','Contador'),
        ('1','Administrador'),
        ('2','Economista'),
        ('3','Otro'),
        
    )
    first_name = models.CharField('Nombres', max_length=60)
    last_name = models.CharField('Apellidos', max_length=60)
    full_name = models.CharField(
        'Nombres Completos', 
        max_length=50,
        blank=True
        )
    job = models.CharField('Trabajo', max_length=50,choices=JOB_CHOICES)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    habilidades = models.ManyToManyField(Habilidades)
    hoja_vida = RichTextField()
    avatar = models.ImageField(upload_to='empleado', blank=True, null=True)
    
    class Meta:
        verbose_name= 'Empleado Administrativo'
        verbose_name_plural= 'Empleados de las Areas'
        ordering =['id']
        unique_together=('first_name','last_name')
    
    def __str__(self) -> str:
        return str(self.id) + ' ' + self.first_name + ' - ' + self.last_name
    